﻿namespace Game
{
    //Author : François-Xavier Bernier
    public enum AnimalEvent
    {
        Nothing,
        Idle,
        Horny,
        Hungry,
        Thirsty,
        Flee
    }
}