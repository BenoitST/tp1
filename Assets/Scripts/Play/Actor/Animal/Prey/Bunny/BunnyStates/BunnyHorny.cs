﻿namespace Game
{
    // Author: Benoit Simon-Turgeon
    public class BunnyHorny : IState
    {
        private Bunny bunny;
        private Bunny nearestBunnyToMate;
        private OffspringCreator offspringCreator;

        public BunnyHorny(Bunny bunny)
        {
            this.bunny = bunny;
        }
        
        public IState Update()
        {
            nearestBunnyToMate = bunny.BunnySensor.SensedObjects.FindNearest(bunny.transform.position, float.MaxValue);
            
            bunny.CreateCouple((Animal) nearestBunnyToMate);

            if (nearestBunnyToMate == null)
            {
                bunny.RandomMover();
            }
            else if (bunny.TryMating((Animal) nearestBunnyToMate))
            {
                return new BunnyIdle(bunny);
            }
            else
            {
                bunny.MoverOnSensoredObject(nearestBunnyToMate);
            }

            return this;
        }

        public void Enter()
        {
        }

        public void Leave()
        {
        }
    }
}