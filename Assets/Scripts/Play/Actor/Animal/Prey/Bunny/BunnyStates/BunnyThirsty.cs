﻿namespace Game
{
    // Author: Benoit Simon-Turgeon
    public class BunnyThirsty : IState
    {
        private Bunny bunny;
        private IDrinkable nearestEntity;

        public BunnyThirsty(Bunny bunny)
        {
            this.bunny = bunny;
        }
        
        public IState Update()
        {
            nearestEntity = bunny.WaterSensor.SensedObjects.FindNearest(bunny.transform.position, float.MaxValue);
            
            if (nearestEntity == null)
            {
                bunny.RandomMover();
            }
            else if (bunny.transform.position == nearestEntity.Position)
            {
                bunny.FeedDrink(nearestEntity);
                return new BunnyIdle(bunny);
            }
            else
                bunny.MoverOnSensoredObject(nearestEntity);

            return this;
        }

        public void Enter()
        {
        }


        public void Leave()
        {
        }
    }
}