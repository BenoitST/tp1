﻿namespace Game
{
    // Author: Benoit Simon-Turgeon
    public class BunnyIdle : IState
    {
        private Bunny bunny;

        public BunnyIdle(Bunny bunny)
        {
            this.bunny = bunny;
        }

        public IState Update()
        {
            AnimalEvent bunnyEvent = bunny.ReadCurrentAnimalEvent();

            switch (bunnyEvent)
            {
                case AnimalEvent.Idle:
                    break;
                case AnimalEvent.Thirsty:
                    return new BunnyThirsty(bunny);

                case AnimalEvent.Hungry:
                    return new BunnyHungry(bunny);

                case AnimalEvent.Horny:
                    return new BunnyHorny(bunny);

                case AnimalEvent.Flee:
                    return new BunnyFlee(bunny);
            }
            bunny.RandomMover();
            
            return this;
        }

        public void Enter()
        {
        }

        public void Leave()
        {
        }
    }
}