﻿namespace Game
{
    // Author: Benoit Simon-Turgeon
    public class BunnyStateMachine : AnimalStateMachine
    {
        private IState currentState;

        public BunnyStateMachine(Bunny bunny)
        {
            currentState = new BunnyIdle(bunny);
        }

        public void Update()
        {
            IState nextState = currentState.Update();
            if (nextState != currentState)
            {
                currentState.Leave();
                currentState = nextState;
                currentState.Enter();
            }
        }
    }
}