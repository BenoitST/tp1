﻿namespace Game
{
    // Author: Benoit Simon-Turgeon
    public class BunnyFlee : IState
    {
        private Bunny bunny;
        private IPredator nearestPredator;

        public BunnyFlee(Bunny bunny)
        {
            this.bunny = bunny;
        }

        public IState Update()
        {
            nearestPredator = bunny.PredatorSensor.SensedObjects.FindNearest(bunny.transform.position);
            
            if (nearestPredator == null)
                return new BunnyIdle(bunny);
            
            bunny.FleeMover(nearestPredator);

            return this;
        }

        public void Enter()
        {
        }

        public void Leave()
        {
        }
    }
}