﻿namespace Game
{
    // Author: Benoit Simon-Turgeon
    public class BunnyHungry : IState
    {
        private Bunny bunny;
        private IEatable nearestEntity;

        public BunnyHungry(Bunny bunny)
        {
            this.bunny = bunny;
        }
        
        public IState Update()
        {
            nearestEntity = bunny.FoodSensor.SensedObjects.FindNearest(bunny.transform.position, float.MaxValue);

            if (nearestEntity == null || !nearestEntity.IsEatable)
            {
                bunny.RandomMover();
            }
            else if (bunny.transform.position == nearestEntity.Position && nearestEntity.IsEatable)
            {
                bunny.FeedFood(nearestEntity);
                return new BunnyIdle(bunny);
            }
            else
                bunny.MoverOnSensoredObject(nearestEntity);

            return this;
        }

        public void Enter()
        {
        }

        public void Leave()
        {
        }
    }
}