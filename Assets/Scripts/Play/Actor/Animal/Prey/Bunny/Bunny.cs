using System;
using Harmony;
using UnityEngine;

namespace Game
{
    // Author: Benoit Simon-Turgeon
    public class Bunny : Animal, IPrey
    {
        [Header("Other")] [SerializeField] [Range(0f, 1f)]
        private float nutritiveValue = 0.5f;
        
        public ISensor<IVegetable> FoodSensor { get; private set; }

        public ISensor<IDrinkable> WaterSensor { get; private set; }

        public ISensor<Bunny> BunnySensor { get; private set; }

        public ISensor<IPredator> PredatorSensor { get; private set; }

        public bool IsEatable => !Vitals.IsDead;

        private BunnyBirthEventChannel bunnyBirthEventChannel;
        private BunnyDeathEventChannel bunnyDeathEventChannel;


        private new void Awake()
        {
            base.Awake();
            bunnyBirthEventChannel = Finder.BunnyBirthEventChannel;
            bunnyDeathEventChannel = Finder.BunnyDeathEventChannel;
            stateMachine = new BunnyStateMachine(this);

            var sensor = Sensor;
            FoodSensor = sensor.For<IVegetable>();
            BunnySensor = sensor.For<Bunny>();
            WaterSensor = sensor.For<IDrinkable>();
            PredatorSensor = sensor.For<IPredator>();
            bunnyBirthEventChannel.Publish(this);
        }

        [ContextMenu("Eat")]
        public IEffect BeEaten()
        {
            if (!IsEatable)
                throw new Exception("You are trying to eat a dead Bunny. " +
                                    "Check if it is eatable before eating it.");

            Vitals.BeEaten();

            return new LoseHungerEffect(nutritiveValue);
        }

        private void OnDestroy()
        {
            bunnyDeathEventChannel.Publish(this);
        }
    }
}