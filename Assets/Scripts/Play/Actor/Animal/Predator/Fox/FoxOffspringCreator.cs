namespace Game
{
    //Author: François-Xavier Bernier
    public class FoxOffspringCreator : OffspringCreator
    {
        protected override Animal CreateOffspringPrefab(Animal otherAnimal)
        {
            return PrefabFactory.CreateFox(Animal.Position, FaunaRoot);
        }
    }
}