using Game.FoxStates;
using Harmony;

namespace Game
{
    //Author : François-Xavier Bernier
    public class Fox : Animal, IPredator
    {
        private ISensor<IPrey> foodSensor;
        private ISensor<IDrinkable> drinkableSensor;
        private ISensor<Fox> hornyMateSensor;

        public ISensor<IPrey> FoodSensor
        {
            get => foodSensor;
            private set => foodSensor = value;
        }

        public ISensor<IDrinkable> DrinkableSensor
        {
            get => drinkableSensor;
            private set => drinkableSensor = value;
        }

        public ISensor<Fox> HornyMateSensor
        {
            get => hornyMateSensor;
            private set => hornyMateSensor = value;
        }

        private FoxBirthEventChannel foxBirthEventChannel;
        private FoxDeathEventChannel foxDeathEventChannel;

        private new void Awake()
        {
            base.Awake();
            stateMachine = new FoxStateMachine(this);

            foxBirthEventChannel = Finder.FoxBirthEventChannel;
            foxDeathEventChannel = Finder.FoxDeathEventChannel;

            var sensor = Sensor;
            FoodSensor = sensor.For<IPrey>();
            DrinkableSensor = sensor.For<IDrinkable>();
            HornyMateSensor = sensor.For<Fox>();
            foxBirthEventChannel.Publish(this);
        }

        private void OnDestroy()
        {
            foxDeathEventChannel.Publish(this);
        }
    }
}