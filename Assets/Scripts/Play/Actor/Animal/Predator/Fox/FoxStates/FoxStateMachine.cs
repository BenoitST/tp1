﻿namespace Game.FoxStates
{
    //Author : François-Xavier Bernier
    public class FoxStateMachine : AnimalStateMachine
    {
        private IState currentState;

        public FoxStateMachine(Fox fox)
        {
            currentState = new FoxIdle(fox);
        }
        
        public void Update()
        {
            IState nextState = currentState.Update();
            if (nextState != currentState)
            {
                currentState.Leave();
                currentState = nextState;
                currentState.Enter();
            }
        }
    }
}