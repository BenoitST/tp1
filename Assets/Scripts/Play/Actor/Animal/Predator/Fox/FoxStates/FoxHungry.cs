﻿namespace Game.FoxStates
{
    //Author : François-Xavier Bernier
    public class FoxHungry : IState
    {
        private Fox fox;
        private IPrey nearestBunny;
        public FoxHungry(Fox fox)
        {
            this.fox = fox;
        }
        
        public IState Update()
        {
            nearestBunny = fox.FoodSensor.SensedObjects.FindNearest(fox.transform.position, float.MaxValue);
            
            if (nearestBunny == null)
            {
                fox.RandomMover();
            }
            else if (fox.transform.position == nearestBunny.Position )
            {
                fox.FeedFood(nearestBunny);
                
                return new FoxIdle(fox);
            }
            else
            {
                fox.MoverOnSensoredObject(nearestBunny);
            }
            return this;
        }
        public void Enter()
        {
        }
        public void Leave()
        {
        }
    }
}