﻿namespace Game.FoxStates
{
    //Author : François-Xavier Bernier
    public class FoxIdle : IState
    {
        private Fox fox;
        public FoxIdle(Fox fox)
        {
            this.fox = fox;
        }
        
        public IState Update()
        {
            AnimalEvent foxEvent = fox.ReadCurrentAnimalEvent();
            fox.RandomMover();
            
            switch (foxEvent)
            {
                case AnimalEvent.Idle:
                    break;
                
                case  AnimalEvent.Thirsty:
                    return new FoxThisty(fox);
                
                case AnimalEvent.Hungry:
                    return new FoxHungry(fox);
                
                case AnimalEvent.Horny:
                    return new FoxHorny(fox);
                
                default:
                    return new FoxIdle(fox);
            }
            return this;
        }
        public void Enter()
        {
        }
        public void Leave()
        {
        }
    }
}