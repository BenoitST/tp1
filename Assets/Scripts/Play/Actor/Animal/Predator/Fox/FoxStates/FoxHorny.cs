﻿namespace Game.FoxStates
{
    //Author : François-Xavier Bernier
    public class FoxHorny : IState
    {
        private Fox fox;
        private Fox nearestFoxToMate;
        private OffspringCreator offspringCreator;
        public FoxHorny(Fox fox)
        {
            this.fox = fox;
        }
        
        public IState Update()
        { 
            nearestFoxToMate = fox.HornyMateSensor.SensedObjects.FindNearest(fox.transform.position, float.MaxValue);
            
            fox.CreateCouple((Animal) nearestFoxToMate);

            if (nearestFoxToMate == null)
            {
                fox.RandomMover();
            }
            else if (fox.TryMating((Animal)nearestFoxToMate))
            {
                return new FoxIdle(fox);
            }
            else
            {
                fox.MoverOnSensoredObject(nearestFoxToMate);
            }
            return this;
        }
        public void Enter()
        {
        }
        public void Leave()
        {
        }  
    }
}