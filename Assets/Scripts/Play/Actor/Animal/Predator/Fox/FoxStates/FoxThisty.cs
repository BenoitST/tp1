﻿namespace Game.FoxStates
{
    //Author : François-Xavier Bernier
    public class FoxThisty : IState
    {
        private Fox fox;
        private IDrinkable nearestEntity;

        public FoxThisty(Fox fox)
        {
            this.fox = fox;
        }
        
        public IState Update()
        {
            
            nearestEntity = fox.DrinkableSensor.SensedObjects.FindNearest(fox.transform.position, float.MaxValue);
            
            if (nearestEntity == null)
            {
                fox.RandomMover();
            }
            else if (fox.transform.position == nearestEntity.Position)
            {
                fox.FeedDrink(nearestEntity);
                 return new FoxIdle(fox);
            }
            else
            {
                fox.MoverOnSensoredObject(nearestEntity);
            }
            return this;
        }
        public void Enter()
        {
        }

        public void Leave()
        {
        }
    }
}