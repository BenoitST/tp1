﻿namespace Game
{
    public interface AnimalStateMachine
    {
        void Update();
    }
}