using Harmony;

namespace Game
{
    public class Memory
    {
        private Animal loveInterest;
        private Animal interestedLover;

        public Animal LoveInterest
        {
            get
            {
                if (loveInterest.IsDestroyed()) loveInterest = null;
                return loveInterest;
            }
            set => loveInterest = value;
        }

        public Animal InterestedLover
        {
            get
            {
                if (interestedLover.IsDestroyed()) interestedLover = null;
                return interestedLover;
            }
            set => interestedLover = value;
            
        }

        public bool HasLoveInterest => LoveInterest != null;
        public bool HasInterestedLover => InterestedLover != null;
    }
}