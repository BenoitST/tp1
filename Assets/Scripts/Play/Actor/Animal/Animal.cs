using System;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    public abstract class Animal : Actor
    {
        [SerializeField] private uint minStepsOnRandomWalk = 1;
        [SerializeField] private uint maxStepsOnRandomWalk = 3;

        private VitalStats vitals;
        private Mover mover;
        private PathFinder pathFinder;
        private Feeder feeder;
        private OffspringCreator offspringCreator;
        private Sensor sensor;
        private Memory memory;

        protected AnimalStateMachine stateMachine;
        private Stack<AnimalEvent> animalEvents;
        private List<Node> currentPath;
        private Node fleePath;

        public VitalStats Vitals => vitals;

        private Mover Mover => mover;

        private PathFinder PathFinder => pathFinder;

        private Feeder Feeder => feeder;

        private OffspringCreator OffspringCreator => offspringCreator;

        protected Sensor Sensor => sensor;

        private Memory Memory => memory;

        protected void Awake()
        {
            pathFinder = Finder.PathFinder;
            vitals = GetComponentInChildren<VitalStats>();
            mover = GetComponentInChildren<Mover>();
            feeder = GetComponentInChildren<Feeder>();
            offspringCreator = GetComponentInChildren<OffspringCreator>();
            sensor = GetComponentInChildren<Sensor>();
            memory = new Memory();
            animalEvents = new Stack<AnimalEvent>();
        }

        private void Update()
        {
            UpdateEventList();
#if UNITY_EDITOR
            try
            {
#endif
                stateMachine.Update();
#if UNITY_EDITOR
            }
            catch (Exception ex)
            {
                Debug.LogError($"{name} errored : {ex.Message}\n{ex.StackTrace}.", gameObject);
                gameObject.SetActive(false);
            }
#endif
        }

        //Author: Francois-Xavier Bernier
        private void UpdateEventList()
        {
            if (Vitals.IsThirsty)
            {
                animalEvents.Push(AnimalEvent.Thirsty);
            }
            else if (Vitals.IsHungry)
            {
                animalEvents.Push(AnimalEvent.Hungry);
            }
            else if (Vitals.IsHorny)
            {
                animalEvents.Push(AnimalEvent.Horny);
            }
            else if (CheckIfPredator())
            {
                animalEvents.Push(AnimalEvent.Flee);
            }
            else
            {
                animalEvents.Push(AnimalEvent.Idle);
            }
        }

        // Author: Benoit Simon-Turgeon
        private bool CheckIfPredator()
        {
            return Sensor.SensedObjects.Any(sensedObject => sensedObject.GetComponent<IPredator>() != null);
        }

        //Author : François-Xavier Bernier
        public AnimalEvent ReadCurrentAnimalEvent()
        {
            return animalEvents.Count == 0 ? AnimalEvent.Nothing : animalEvents.Pop();
        }

        private void OnEnable()
        {
            vitals.OnDeath += OnDeath;

            if (vitals.IsDead) OnDeath();
        }

        private void OnDisable()
        {
            vitals.OnDeath -= OnDeath;
        }

        private void OnDeath()
        {
            Destroy();
        }

        [ContextMenu("Destroy")]
        private void Destroy()
        {
            Destroy(gameObject);
        }

        // Author: Benoit Simon-Turgeon
        public void RandomMover()
        {
            if (Mover.IsMoving)
                return;

            if (currentPath?.Any() != true)
                currentPath = PathFinder.FindRandomWalk(transform.position, (int) minStepsOnRandomWalk, (int) maxStepsOnRandomWalk);

            if (currentPath?.Any() == true)
                MoveToNextStep();
        }


        // François-Xavier Bernier
        public void MoverOnSensoredObject(IEntity entityToMoveTo)
        {
            if (Mover.IsMoving)
                return;

            if (currentPath?.Any() != true)
                currentPath = PathFinder.FindPath(transform.position, entityToMoveTo.Position);

            if (currentPath?.Any() == true)
                MoveToNextStep();
        }

        //Author : Benoit Simon-Turgeon
        public void FleeMover(IEntity entityToFlee)
        {
            if (Mover.IsMoving)
                return;

            if (fleePath == null)
                fleePath = PathFinder.FindFleePath(transform.position, entityToFlee.Position);

            if (fleePath == null)
                return;

            Mover.MoveTo(fleePath.Position3D);
            fleePath = null;
        }

        //Author: Benoit Simon-Turgeon
        private void MoveToNextStep()
        {
            Mover.MoveTo(currentPath.First().Position3D);
            currentPath.RemoveAt(0);
        }

        // Author: Benoit Simon-Turgeon
        public void FeedDrink(IDrinkable drinkSource)
        {
            Feeder.Drink(drinkSource);
        }

        // Author: Benoit Simon-Turgeon
        public void FeedFood(IEatable foodSource)
        {
            if (foodSource.IsEatable)
                Feeder.Eat(foodSource);
        }

        //Author : François-Xavier Bernier
        public bool TryMating(Animal otherAnimal)
        {
            if (Memory.InterestedLover != otherAnimal || !OffspringCreator.IsInReach(otherAnimal))
                return false;

            OffspringCreator.CreateOffspringWith(otherAnimal);
            return true;
        }

        //Author : François-Xavier Bernier
        public void CreateCouple(Animal otherAnimal)
        {
            Memory.InterestedLover = otherAnimal;
        }
    }
}