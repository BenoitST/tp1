﻿using UnityEngine;

namespace Game
{
    public class FoxDeathEventChannel : MonoBehaviour
    {
        public event FoxDeathEvent OnFoxDeath;

        public void Publish(Fox fox)
        {
            if (OnFoxDeath != null) OnFoxDeath(fox);
        }
    }

    public delegate void FoxDeathEvent(Fox sender);
}