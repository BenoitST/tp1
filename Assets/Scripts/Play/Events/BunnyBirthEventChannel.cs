﻿using UnityEngine;

namespace Game
{
    public class BunnyBirthEventChannel : MonoBehaviour
    {
        public event BunnyBirthEvent OnBunnyBirth;

        public void Publish(Bunny bunny)
        {
            if (OnBunnyBirth != null) OnBunnyBirth(bunny);
        }
    }

    public delegate void BunnyBirthEvent(Bunny sender);
}