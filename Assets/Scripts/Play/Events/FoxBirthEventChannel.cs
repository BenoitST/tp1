﻿using UnityEngine;

namespace Game
{
    public class FoxBirthEventChannel : MonoBehaviour
    {
        public event FoxBirthEvent OnFoxBirth;

        public void Publish(Fox fox)
        {
            if (OnFoxBirth != null) OnFoxBirth(fox);
        }
    }

    public delegate void FoxBirthEvent(Fox sender);
}