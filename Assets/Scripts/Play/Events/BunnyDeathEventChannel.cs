﻿using UnityEngine;

namespace Game
{
    public class BunnyDeathEventChannel : MonoBehaviour
    {
        public event BunnyDeathEvent OnBunnyDeath;

        public void Publish(Bunny bunny)
        {
            if (OnBunnyDeath != null) OnBunnyDeath(bunny);
        }
    }

    public delegate void BunnyDeathEvent(Bunny sender);
}