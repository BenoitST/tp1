﻿using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "New Music Clip", menuName = "Game/Music CLip")]
    public class MusicClip : ScriptableObject
    {
        [SerializeField] private AudioClip audioClip;
        [SerializeField] [Range(0, 1)] private float volume = 0.4f;

        public AudioClip AudioClip => audioClip;
        public float Volume => volume;
    }
}