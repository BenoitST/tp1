﻿using Harmony;
using UnityEngine;

namespace Game
{
    public class Sound : MonoBehaviour
    {
        [SerializeField] private SoundSettings settings;

        private new VirtualCamera camera;
        private AudioSource audioSource;

        public SoundSettings SoundSettings
        {
            get => settings;
            set => settings = value;
        }

        private Vector2 ViewportPosition => camera.WorldToViewport(transform.position);
        private float Zoom => camera.OrthographicSize;

        private void Awake()
        {
            camera = Finder.GameVirtualCamera;
            audioSource = gameObject.AddComponent<AudioSource>();

            audioSource.volume = 0;
            audioSource.loop = false;
            audioSource.playOnAwake = false;
        }

        private void Update()
        {
            if (!settings) return;

            var viewportPosition = ViewportPosition;
            var zoom = Zoom;

            //Set volume according to viewport visibility and camera zoom.
            //If not in viewport, volume decreases.
            //If zoomed far away, volume decreases.
            audioSource.volume = settings.Volume * GetViewportVisibilityFactor(viewportPosition) * GetZoomDistanceFactor(zoom);

            //Pan the sound according to his sound position on the screen
            //-1 => Left
            //1 => Right
            audioSource.panStereo = GetStereoPan(viewportPosition);
        }

        private float GetViewportVisibilityFactor(Vector2 viewportPosition)
        {
            //Distance from the viewport edges. Viewport is 1 unit wide and 1 unit tall.
            //Threshold is how far from theses edges the object is still considered inside the viewport.
            //Inside the viewport, the volume is 1.
            var distanceFromViewportX = Mathf.Abs(0.5f - viewportPosition.x) - 0.5f - settings.VisibilityDistanceThreshold;
            var distanceFromViewportY = Mathf.Abs(0.5f - viewportPosition.y) - 0.5f - settings.VisibilityDistanceThreshold;

            //How much the object is in the viewport ?
            //Falloff is the distance over which the volume declines. Inside the falloff, the volume decreases linearly.
            //Past the falloff, volume is 0.
            var viewportVisibilityFactorX = Mathf.Clamp01(distanceFromViewportX / settings.VisibilityDistanceFalloff);
            var viewportVisibilityFactorY = Mathf.Clamp01(distanceFromViewportY / settings.VisibilityDistanceFalloff);

            return Mathf.Clamp01(1 - (viewportVisibilityFactorX + viewportVisibilityFactorY));
        }

        private float GetZoomDistanceFactor(float zoom)
        {
            //How much the camara zoom impact the volume ?
            //Falloff is the distance over which the volume declines. Inside the falloff, the volume decreases linearly.
            //Past the falloff, volume is 0.
            //Threshold is the minimal zoom value. At this zoom exact zoom level, volume is 1.
            return Mathf.InverseLerp(
                settings.ZoomDistanceFalloff + settings.ZoomDistanceThreshold,
                settings.ZoomDistanceThreshold,
                zoom
            );
        }

        private float GetStereoPan(Vector2 viewportPosition)
        {
            return Mathf.Lerp(-1, 1, viewportPosition.x);
        }

        public void Play(AudioClip audioClip)
        {
            audioSource.PlayOneShot(audioClip);
        }
    }
}