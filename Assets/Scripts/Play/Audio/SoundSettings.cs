﻿using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "New Sound Settings", menuName = "Game/Sound Settings", order = 0)]
    public class SoundSettings : ScriptableObject
    {
        [Header("Common")] [SerializeField] [Range(0, 1)] private float volume = 1f;
        [Header("Spatial Audio")] [SerializeField] [Min(0)] private float zoomDistanceThreshold = 0.2f;
        [SerializeField] [Min(0)] private float zoomDistanceFalloff = 4f;
        [SerializeField] [Min(0)] private float visibilityDistanceThreshold = 0.1f;
        [SerializeField] [Min(float.Epsilon)] private float visibilityDistanceFalloff = 0.2f;

        public float Volume => volume;
        public float ZoomDistanceThreshold => zoomDistanceThreshold;
        public float ZoomDistanceFalloff => zoomDistanceFalloff;
        public float VisibilityDistanceThreshold => visibilityDistanceThreshold;
        public float VisibilityDistanceFalloff => visibilityDistanceFalloff;
    }
}