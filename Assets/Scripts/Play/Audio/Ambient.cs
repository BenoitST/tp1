﻿using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.Ambient)]
    public class Ambient : MonoBehaviour
    {
        [SerializeField] private AmbientClip[] ambientClips;

        private new VirtualCamera camera;
        private AudioSource[] audioSources;

        private void Awake()
        {
            camera = Finder.GameVirtualCamera;

            audioSources = new AudioSource[ambientClips.Length];
            for (var i = 0; i < ambientClips.Length; i++)
            {
                var ambientClip = ambientClips[i];
                var audioSource = gameObject.AddComponent<AudioSource>();
                audioSources[i] = audioSource;

                audioSource.clip = ambientClip.AudioClip;
                audioSource.loop = true;
            }
        }

        private void OnEnable()
        {
            foreach (var audioSource in audioSources)
                audioSource.Play();
        }

        private void OnDisable()
        {
            foreach (var audioSource in audioSources)
                audioSource.Stop();
        }

        private void Update()
        {
            var zoom = camera.OrthographicSize;

            for (var i = 0; i < ambientClips.Length; i++)
            {
                var ambientClip = ambientClips[i];
                var audioSource = audioSources[i];

                audioSource.volume = ambientClip.Volume * ambientClip.ZoomVolumeCurve.Evaluate(zoom);
            }
        }
    }
}