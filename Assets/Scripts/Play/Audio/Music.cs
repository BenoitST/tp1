﻿using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.Music)]
    public class Music : MonoBehaviour
    {
        [SerializeField] private MusicClip[] musicClips;
        
        private AudioSource audioSource;

        private void Awake()
        {
            audioSource = gameObject.AddComponent<AudioSource>();

            var musicClip = musicClips.Random();
            audioSource.clip = musicClip.AudioClip;
            audioSource.volume = musicClip.Volume;
            audioSource.loop = true;
        }

        private void OnEnable()
        {
            audioSource.Play();
        }

        private void OnDisable()
        {
            audioSource.Stop();
        }
    }
}