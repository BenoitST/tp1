﻿using UnityEngine;

namespace Game
{
    [CreateAssetMenu(fileName = "New Ambient Clip", menuName = "Game/Ambient Clip")]
    public class AmbientClip : ScriptableObject
    {
        [SerializeField] private AudioClip audioClip;
        [SerializeField] [Range(0, 1)] private float volume = 1f;
        [SerializeField] private AnimationCurve zoomVolumeCurve;

        public AudioClip AudioClip => audioClip;
        public float Volume => volume;
        public AnimationCurve ZoomVolumeCurve => zoomVolumeCurve;
    }
}