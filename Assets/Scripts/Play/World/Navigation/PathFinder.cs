using System;
using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;
using Random = System.Random;

namespace Game
{
    [Findable(Tags.NavigationMesh)]
    public sealed class PathFinder : MonoBehaviour
    {
        private RandomSeed randomSeed;
        private NavigationMesh navigationMesh;
        private Random random;

        private void Awake()
        {
            randomSeed = Finder.RandomSeed;
            navigationMesh = Finder.NavigationMesh;
            
            //Default one. Updated when the navigation mesh is created.
            CreateRandom(); 
            
            navigationMesh.OnChanged += CreateRandom;
        }

        private void OnDestroy()
        {
            navigationMesh.OnChanged -= CreateRandom;
        }

        private void CreateRandom()
        {
            random = randomSeed.CreateRandom();
        }

        public List<Node> FindPath(Vector3 start, Vector3 end)
        {
            var startNode = navigationMesh.Find(start);
            if (startNode == null) return null;

            var endNode = navigationMesh.Find(end);
            if (endNode == null) return null;

            //A* algorithm.
            
            //From each node, which node it can most efficiently be reached from.
            var previous = new Dictionary<Node, Node>();
            //Known nodes not yet evaluated.
            var openedNodes = new HashSet<Node>();
            //Evaluated nodes
            var closedNodes = new HashSet<Node>();
            //Cost from start node to a specific node.
            var costToNode = new Dictionary<Node, float>();
            //Cost from start node to end node, passing by a specific node.
            var costToEnd = new Dictionary<Node, float>();

            //Cost to start node is 0.
            openedNodes.Add(startNode);
            costToNode[startNode] = 0;
            costToEnd[startNode] = Vector2.Distance(startNode.Position2D, endNode.Position2D);

            bool pathFound = false;
            while (openedNodes.Count > 0)
            {
                var currentNode = GetLeastCostToEndNode(openedNodes, costToEnd);

                if (currentNode == endNode)
                {
                    pathFound = true;
                    break;
                }

                openedNodes.Remove(currentNode);
                closedNodes.Add(currentNode);

                var costToCurrentNode = costToNode.ContainsKey(currentNode) ? costToNode[currentNode] : float.MaxValue;

                foreach (var neighbourNode in currentNode.Neighbours)
                {
                    if (!closedNodes.Contains(neighbourNode))
                    {
                        openedNodes.Add(neighbourNode); //openedNodes is "HashSet". Therefore, it can't have duplicates.

                        var newCostToNeighbour = costToCurrentNode +
                                                 Vector2.Distance(currentNode.Position2D, neighbourNode.Position2D);

                        var costToNeighbourNode = costToNode.ContainsKey(neighbourNode)
                            ? costToNode[neighbourNode]
                            : float.MaxValue;
                        if (newCostToNeighbour < costToNeighbourNode)
                        {
                            var neighbourCostToEnd = newCostToNeighbour +
                                                     Vector2.Distance(currentNode.Position2D, endNode.Position2D);

                            previous[neighbourNode] = currentNode;
                            costToNode[neighbourNode] = newCostToNeighbour;
                            costToEnd[neighbourNode] = neighbourCostToEnd;
                        }
                    }
                }
            }

            return pathFound ? GetPathFromPrevious(previous, endNode) : null;
        }

        public List<Node> FindRandomWalk(Vector3 start, int minSteps, int maxSteps)
        {
            if (minSteps > maxSteps)
                throw new ArgumentException($"{nameof(minSteps)} must be lower or equal to {nameof(maxSteps)}");

            var startNode = navigationMesh.Find(start);
            if (startNode == null) return null;

            var path = new List<Node>();
            var pathOptions = new List<Node>();

            var nbSteps = random.Next(minSteps, maxSteps);
            var currentNode = startNode;
            var sqrDistanceFromStart = 0f;
            for (var i = 0; i < nbSteps && currentNode != null; i++)
            {
                pathOptions.AddRange(currentNode.Neighbours);

                while (pathOptions.Count > 0)
                {
                    currentNode = pathOptions.RemoveRandom(random);
                    var sqDistanceToCurrentNode = startNode.Position2D.SqrDistanceTo(currentNode.Position2D);
                    if (sqDistanceToCurrentNode > sqrDistanceFromStart)
                    {
                        path.Add(currentNode);
                        sqrDistanceFromStart = sqDistanceToCurrentNode;
                        break;
                    }

                    //No more options are available to go farther.
                    if (pathOptions.Count == 0)
                    {
                        currentNode = null;
                        break;
                    }
                }

                pathOptions.Clear();
            }

            return path;
        }

        public Node FindFleePath(Vector3 start, Vector3 toFlee)
        {
            var startNode = navigationMesh.Find(start);
            if (startNode == null) return null;

            var sqrDistanceFleeToStart = toFlee.SqrDistanceTo(start);
            var fleeOptions = startNode.Neighbours.ToList();
            while (fleeOptions.Count > 0)
            {
                var currentNode = fleeOptions.RemoveRandom(random);
                if (toFlee.SqrDistanceTo(currentNode.Position3D) > sqrDistanceFleeToStart)
                {
                    return currentNode;
                }
            }

            return null;
        }

        private static Node GetLeastCostToEndNode(
            IEnumerable<Node> openedNodes,
            IReadOnlyDictionary<Node, float> costToEnd
        )
        {
            var leastCost = float.MaxValue;
            Node leastCostNode = null;
            foreach (var currentNode in openedNodes)
            {
                var nodeCostToEnd = costToEnd.ContainsKey(currentNode) ? costToEnd[currentNode] : float.MaxValue;
                if (nodeCostToEnd < leastCost)
                {
                    leastCost = nodeCostToEnd;
                    leastCostNode = currentNode;
                }
            }

            return leastCostNode;
        }

        private static List<Node> GetPathFromPrevious(
            IReadOnlyDictionary<Node, Node> previous,
            Node endNode
        )
        {
            var path = new List<Node>();

            var currentNode = endNode;
            path.Add(endNode);

            while (previous.ContainsKey(currentNode))
            {
                currentNode = previous[currentNode];
                path.Insert(0, currentNode);
            }

            path.RemoveAt(0); //Remove first, because that's the start of the path, and we're already there.

            return path;
        }
    }
}