using System.Collections;
using DG.Tweening;
using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.Terrain)]
    public class TerrainAnimator : MonoBehaviour
    {
        [SerializeField] private float spawnAnimDuration = 0.5f;

        private Terrain terrain;

        private void Awake()
        {
            terrain = Finder.Terrain;
        }

        public Coroutine PlaySpawnAnimation()
        {
            IEnumerator Routine()
            {
                var terrainTransform = terrain.ObjectsRoot.transform;
                
                var startScale = Vector3.zero;
                var endScale = Vector3.one;
                terrainTransform.localScale = startScale;

                yield return terrainTransform.DOScale(endScale, spawnAnimDuration).WaitForCompletion();
            }

            terrain.ObjectsRoot.SetActive(true);
            return StartCoroutine(Routine());
        }
        
        public void PrepareSpawnAnimation()
        {
            terrain.ObjectsRoot.SetActive(false);
        }
    }
}