using UnityEngine;

namespace Game
{
    public class Water : MonoBehaviour, IDrinkable
    {
        [SerializeField] private float nutritiveValue = 1f;

        public Vector3 Position => transform.position;

        public IEffect Drink()
        {
            return new LoseThirstEffect(nutritiveValue);
        }
    }
}