using System.Collections;
using DG.Tweening;
using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.Fauna)]
    public class FaunaAnimator : MonoBehaviour
    {
        [SerializeField] private float spawnAnimDuration = 0.5f;

        private FaunaGenerator faunaGenerator;

        private void Awake()
        {
            faunaGenerator = Finder.FaunaGenerator;
        }
        
        public Coroutine PlaySpawnAnimation()
        {
            IEnumerator Routine()
            {
                var sequence = DOTween.Sequence();
                foreach (var fauna in faunaGenerator.ObjectsRoot.Children())
                {
                    var faunaTransform = fauna.transform;

                    var startScale = Vector3.zero;
                    var endScale = Vector3.one;
                    faunaTransform.localScale = startScale;

                    sequence.Join(faunaTransform.DOScale(endScale, spawnAnimDuration));
                }

                yield return sequence.WaitForCompletion();
            }

            faunaGenerator.ObjectsRoot.SetActive(true);
            return StartCoroutine(Routine());
        }

        public void PrepareSpawnAnimation()
        {
            faunaGenerator.ObjectsRoot.SetActive(false);
        }
    }
}