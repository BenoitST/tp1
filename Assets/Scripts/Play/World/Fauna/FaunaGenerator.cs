using System.Collections.Generic;
using System.Linq;
using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.Fauna)]
    public class FaunaGenerator : MonoBehaviour
    {
        private const string FAUNA_ROOT_NAME = "FaunaGameObjects";

        [SerializeField] [Range(0f, 1f)] private float bunniesDensity = 0.05f;
        [SerializeField] [Range(0f, 1f)] private float foxDensity = 0.005f;

        private PrefabFactory prefabFactory;

        private GameObject faunaRoot;

        public GameObject ObjectsRoot => faunaRoot;

        private void Awake()
        {
            prefabFactory = Finder.PrefabFactory;

            faunaRoot = CreateRoot(FAUNA_ROOT_NAME);
        }

        private GameObject CreateRoot(string gameObjectName)
        {
            var rootGameObject = new GameObject(gameObjectName);
            rootGameObject.transform.parent = transform;
            return rootGameObject;
        }

        public void Generate(RandomSeed randomSeed, NavigationMesh navigationMesh)
        {
            DestroyFauna();
            CreateFauna(randomSeed, navigationMesh);
        }

        private void CreateFauna(RandomSeed randomSeed, NavigationMesh navigationMesh)
        {
            var nodes = navigationMesh.Nodes.ToList();
            var nbGrassBlocks = nodes.Count;

            var faunaPrefabs = new Dictionary<int, int>
            {
                [0] = (int) (nbGrassBlocks * bunniesDensity),
                [1] = (int) (nbGrassBlocks * foxDensity),
            };

            var random = randomSeed.CreateRandom();

            while (faunaPrefabs.Count > 0 && nodes.Count > 0)
            {
                var position = nodes.RemoveRandom(random).Position3D;
                var prefab = faunaPrefabs.SubtractRandom(random);

                if (prefab == 0) prefabFactory.CreateBunny(position, faunaRoot);
                else prefabFactory.CreateFox(position, faunaRoot);
            }
        }

        private void DestroyFauna()
        {
            for (var i = 0; i < faunaRoot.transform.childCount; i++)
                Destroy(faunaRoot.transform.GetChild(i).gameObject);
        }
    }
}