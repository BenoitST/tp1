using System.Collections;
using DG.Tweening;
using Harmony;
using UnityEngine;

namespace Game
{
    [Findable(Tags.Flora)]
    public class FloraAnimator : MonoBehaviour
    {
        [SerializeField] private float spawnAnimDuration = 0.5f;

        private Flora flora;

        private void Awake()
        {
            flora = Finder.Flora;
        }

        public Coroutine PlaySpawnAnimation()
        {
            IEnumerator Routine()
            {
                var sequence = DOTween.Sequence();
                foreach (var flora in flora.ObjectsRoot.Children())
                {
                    var floraTransform = flora.transform;

                    var startScale = Vector3.zero;
                    var endScale = Vector3.one;
                    floraTransform.localScale = startScale;

                    sequence.Join(floraTransform.DOScale(endScale, spawnAnimDuration));
                }

                yield return sequence.WaitForCompletion();
            }

            flora.ObjectsRoot.SetActive(true);
            return StartCoroutine(Routine());
        }

        public void PrepareSpawnAnimation()
        {
            flora.ObjectsRoot.SetActive(false);
        }
    }
}