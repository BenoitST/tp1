using Harmony;
using UnityEngine;

namespace Game
{
    public class OrthographicOrbitCamera : MonoBehaviour
    {
        [Header("Initialisation")] [Range(1, 100)] [SerializeField] private float initialZoom = 20;

        [Header("Position")] [Range(1, 100)] [SerializeField] private float distance = 45f;
        [SerializeField] [Range(0, 90)] private float verticalAngle = 25f;
        [SerializeField] [Range(0, 360)] private float horizontalAngle = 45f;
        [SerializeField] [Range(1, 10)] private int rotationCount = 4;

        [Header("Movement")] [SerializeField] [Range(0, 10)] private float movementSpeed = 2f;
        [SerializeField] [Range(0f, 10f)] private float zoomSpeed = 1f;
        [SerializeField] [Range(0, 10)] private float rotationSpeed = 5f;
        [SerializeField] [Range(0, 10)] private float runMovementSpeedMultiplier = 2f;
        [SerializeField] [Range(0, 10)] private float runZoomSpeedMultiplier = 2f;
        [SerializeField] [Range(0, 10)] private float runRotationSpeedMultiplier = 2f;
        [SerializeField] [Range(0, 100)] private float minZoom = 1;
        [SerializeField] [Range(0, 100)] private float maxZoom = 50;

        private InputActions.GameActions gameInputs;
        private new VirtualCamera camera;

        private Vector3 tracking;
        private int panning;
        private float panningAngle;
        private float zoom;

        private void Awake()
        {
            gameInputs = Finder.Inputs.Actions.Game;
            camera = GetComponent<VirtualCamera>();
        }

        private void OnEnable()
        {
            camera.IsOrthographic = true;

            tracking = Vector3.zero;
            panning = 0;
            panningAngle = 0f;
            zoom = initialZoom;
        }

        private void OnDisable()
        {
            camera.IsOrthographic = false;
        }

        private void Update()
        {
            var cameraInputs = ReadInputs();

            UpdateTracking(cameraInputs);
            UpdatePanning(cameraInputs);
            UpdateZoom(cameraInputs);

            MoveCamera();
        }

        private CameraInputs ReadInputs()
        {
            return new CameraInputs
            {
                TrackingDirection = gameInputs.Move.ReadValue<Vector2>(),
                PanningDirection = gameInputs.Rotate.triggered ? gameInputs.Rotate.ReadValue<float>().RoundToInt() : 0,
#if !PLATFORM_STANDALONE_LINUX
                ZoomDelta = -gameInputs.Zoom.ReadValue<float>(),
#else
                ZoomDelta = gameInputs.Zoom.ReadValue<float>() * 100, //BUG : Reversed on Linux. Also very small. Known bug.
#endif
                IsRunning = gameInputs.Run.ReadValue<float>() > 0f
            };
        }

        private void UpdateTracking(CameraInputs cameraInputs)
        {
            var trackingDirection = cameraInputs.TrackingDirection;
            var isRunning = cameraInputs.IsRunning;

            var speed = movementSpeed * (isRunning ? runMovementSpeedMultiplier : 1f);

            tracking += trackingDirection * (speed * zoom * Time.unscaledDeltaTime);
        }

        private void UpdatePanning(CameraInputs cameraInputs)
        {
            var panningDirection = cameraInputs.PanningDirection;
            var isRunning = cameraInputs.IsRunning;

            var speed = rotationSpeed * (isRunning ? runRotationSpeedMultiplier : 1f);

            panning += panningDirection;
            panningAngle = Mathf.Lerp(panningAngle, panning * (360f / rotationCount), speed * Time.unscaledDeltaTime);
        }

        private void UpdateZoom(CameraInputs cameraInputs)
        {
            var zoomDelta = cameraInputs.ZoomDelta;
            var isRunning = cameraInputs.IsRunning;

            var speed = isRunning ? runZoomSpeedMultiplier : zoomSpeed;

            zoom = Mathf.Clamp(zoom - zoomDelta * speed * Time.unscaledDeltaTime, minZoom, maxZoom);
        }

        private void MoveCamera()
        {
            var trackingRotation = Quaternion.Euler(verticalAngle, horizontalAngle, 0);
            var panningRotation = Quaternion.Euler(0, panningAngle, 0);
            var rotation = panningRotation * trackingRotation;
            var trackingDistance = new Vector3(0, 0, -distance);

            camera.Position = rotation * (tracking + trackingDistance);
            camera.Rotation = rotation;
            camera.OrthographicSize = zoom;
        }

        private struct CameraInputs
        {
            public Vector3 TrackingDirection;
            public int PanningDirection;
            public float ZoomDelta;
            public bool IsRunning;
        }
    }
}