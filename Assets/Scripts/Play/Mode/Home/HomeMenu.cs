using System.Collections;
using System.Linq;
using Harmony;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class HomeMenu : MonoBehaviour
    {
        private Main main;
        private InputActions.MenuActions menuInputs;
        private HomeMenuAnimator homeMenuAnimator;
        private Button playButton;
        private Button quitButton;

        private void Awake()
        {
            main = Finder.Main;
            menuInputs = Finder.Inputs.Actions.Menu;
            homeMenuAnimator = GetComponent<HomeMenuAnimator>();
            var buttons = GetComponentsInChildren<Button>();
            playButton = buttons.WithName(GameObjects.Play);
            quitButton = buttons.WithName(GameObjects.Quit);
        }

        private void Start()
        {
            menuInputs.Enable();
            playButton.Select();
            homeMenuAnimator.PlayOpenAnimation();
        }

        private void OnEnable()
        {
            playButton.onClick.AddListener(StartGame);
            quitButton.onClick.AddListener(QuitApplication);
        }

        private void OnDisable()
        {
            playButton.onClick.RemoveListener(StartGame);
            quitButton.onClick.RemoveListener(QuitApplication);
        }

        private void StartGame()
        {
            IEnumerator Routine()
            {
                main.LoadGameScenes();
                yield return homeMenuAnimator.PlayCloseAnimation();
                main.UnloadHomeScenes();
            }

            StartCoroutine(Routine());
        }

        private void QuitApplication()
        {
            ApplicationExtensions.Quit();
        }
    }
}