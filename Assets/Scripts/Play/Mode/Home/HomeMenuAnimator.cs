using System.Collections;
using DG.Tweening;
using Harmony;
using UnityEngine;

namespace Game
{
    public class HomeMenuAnimator : MonoBehaviour
    {
        [Header("Animations")] [SerializeField] private float openAnimDuration = 1f;
        [SerializeField] private float closeAnimDuration = 1f;
        [Header("Other")] [SerializeField] private float closeDelay = 1f;

        private RectTransform canvas;
        private RectTransform anchor;

        private void Awake()
        {
            canvas = GetComponent<RectTransform>();
            anchor = GetComponentsInChildren<RectTransform>().WithName(GameObjects.Anchor);
        }

        public Coroutine PlayOpenAnimation()
        {
            IEnumerator Routine()
            {
                var endPosition = anchor.anchoredPosition;
                var startPosition = new Vector2(endPosition.x, canvas.anchoredPosition.y + anchor.rect.height / 2);

                anchor.anchoredPosition = startPosition;
                yield return anchor.DOAnchorPos(endPosition, openAnimDuration)
                    .SetEase(Ease.OutQuad)
                    .WaitForCompletion();
            }

            return StartCoroutine(Routine());
        }

        public Coroutine PlayCloseAnimation()
        {
            IEnumerator Routine()
            {
                var startPosition = anchor.anchoredPosition;
                var endPosition = new Vector2(startPosition.x, canvas.anchoredPosition.y + anchor.rect.height / 2);

                anchor.anchoredPosition = startPosition;
                yield return anchor.DOAnchorPos(endPosition, openAnimDuration)
                    .SetEase(Ease.OutQuad)
                    .WaitForCompletion();

                yield return new WaitForSeconds(closeDelay);
            }

            return StartCoroutine(Routine());
        }
    }
}