using Harmony;
using UnityEngine;

namespace Game
{
    public class GameTime : MonoBehaviour
    {
        [SerializeField] [Range(0f, 10f)] private float initialTimeScale = 1f;
        [SerializeField] [Range(0f, 10f)] private float minTimeScale = 0f;
        [SerializeField] [Range(0f, 10f)] private float maxTimeScale = 10f;
        [SerializeField] [Range(0f, 10f)] private float timeScaleIncrement = 1f;
        
        private InputActions.GameActions gameInputs;
        
        private void Awake()
        {
            gameInputs = Finder.Inputs.Actions.Game;
        }

        private void Start()
        {
            Time.timeScale = initialTimeScale;
        }

        private void Update()
        {
            if (gameInputs.SpeedUp.triggered)
                Time.timeScale = Mathf.Clamp(Time.timeScale + timeScaleIncrement, minTimeScale, maxTimeScale);
            if (gameInputs.SpeedDown.triggered)
                Time.timeScale = Mathf.Clamp(Time.timeScale - timeScaleIncrement, minTimeScale, maxTimeScale);
        }
    }
}