﻿using DataBase;
using Harmony;
using UnityEngine;

namespace Game
{
    public class GameGenerator : MonoBehaviour
    {
        [SerializeField] private bool generateTerrain = true;
        [SerializeField] private bool generateFlora = true;
        [SerializeField] private bool generateWater = true;
        [SerializeField] private bool generateNavigationMesh = true;
        [SerializeField] private bool generateFauna = true;
        
        private RandomSeed randomSeed;
        private Terrain terrain;
        private Flora flora;
        private NavigationMesh navigationMesh;

        private TerrainGenerator terrainGenerator;
        private WaterGenerator waterGenerator;
        private FloraGenerator floraGenerator;
        private NavigationMeshGenerator navigationMeshGenerator;
        private FaunaGenerator faunaGenerator;

        private void Awake()
        {
            randomSeed = Finder.RandomSeed;

            terrain = Finder.Terrain;
            flora = Finder.Flora;
            navigationMesh = Finder.NavigationMesh;

            terrainGenerator = Finder.TerrainGenerator;
            waterGenerator = Finder.WaterGenerator;
            floraGenerator = Finder.FloraGenerator;
            navigationMeshGenerator = Finder.NavigationMeshGenerator;
            faunaGenerator = Finder.FaunaGenerator;
        }
        
        public void Generate()
        {
            /*
             * Generation goes like this.
             * 
             * 1. Generate terrain.
             * 2. Using terrain, put water.
             * 3. Using terrain, put flora.
             * 4. Using terrain and flora, generate navigation mesh.
             * 5. Using navigation mesh, put fauna.
             * 
             * Some steps depends on previous ones. For example, fauna can't be
             * placed without a navigation mesh (to place animal only on walkable spaces).
             */
            
            
            if (generateTerrain)
            {
                terrainGenerator.Generate(randomSeed);
                if (generateWater) waterGenerator.Generate(terrain);
                if (generateFlora) floraGenerator.Generate(randomSeed, terrain);

                if (generateNavigationMesh)
                {
                    navigationMeshGenerator.Generate(terrain, flora);
                    if (generateFauna) faunaGenerator.Generate(randomSeed, navigationMesh);
                }
            }
        }
    }
}