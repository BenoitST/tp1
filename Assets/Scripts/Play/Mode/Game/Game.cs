using Harmony;
using UnityEngine;

namespace Game
{
    public class Game : MonoBehaviour
    {
        private GameGenerator gameGenerator;
        private GameAnimator gameAnimator;
        private InputActions.GameActions gameInputs;

        private void Awake()
        {
            gameGenerator = GetComponent<GameGenerator>();
            gameAnimator = GetComponent<GameAnimator>();
            gameInputs = Finder.Inputs.Actions.Game;
        }

        private void Start()
        {
            gameInputs.Enable();
            gameGenerator.Generate();
            gameAnimator.PlaySpawnAnimations();
        }

        private void Update()
        {
            if (gameInputs.Exit.triggered) ApplicationExtensions.Quit();
        }
    }
}