﻿using System.Collections;
using Harmony;
using UnityEngine;

namespace Game
{
    public class GameAnimator : MonoBehaviour
    {
        private TerrainAnimator terrainAnimator;
        private FloraAnimator floraAnimator;
        private FaunaAnimator faunaAnimator;

        private void Awake()
        {
            terrainAnimator = Finder.TerrainAnimator;
            floraAnimator = Finder.FloraAnimator;
            faunaAnimator = Finder.FaunaAnimator;
        }

        public Coroutine PlaySpawnAnimations()
        {
            IEnumerator Routine()
            {
                terrainAnimator.PrepareSpawnAnimation();
                floraAnimator.PrepareSpawnAnimation();
                faunaAnimator.PrepareSpawnAnimation();

                yield return terrainAnimator.PlaySpawnAnimation();
                floraAnimator.PlaySpawnAnimation();
                faunaAnimator.PlaySpawnAnimation();
            }

            return StartCoroutine(Routine());
        }
    }
}