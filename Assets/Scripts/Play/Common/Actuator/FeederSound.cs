﻿using Harmony;
using UnityEngine;

namespace Game
{
    public class FeederSound : MonoBehaviour
    {
        [Header("Audio Clips")] [SerializeField] private AudioClip[] clips;
        [Header("Spatial audio")] [SerializeField] private SoundSettings settings;

        private Sound sound;
        private Feeder feeder;

        private void Awake()
        {
            sound = gameObject.AddComponent<Sound>();
            feeder = GetComponent<Feeder>();

            sound.SoundSettings = settings;
        }

        private void OnEnable()
        {
            feeder.OnAte += Play;
        }

        private void OnDisable()
        {
            feeder.OnAte -= Play;
        }

        private void Play()
        {
            sound.Play(clips.Random());
        }
    }
}