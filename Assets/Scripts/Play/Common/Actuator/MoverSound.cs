﻿using Harmony;
using UnityEngine;

namespace Game
{
    public class MoverSound : MonoBehaviour
    {
        [Header("Audio Clips")] [SerializeField] private AudioClip[] clips;
        [Header("Spatial audio")] [SerializeField] private SoundSettings settings;

        private Sound sound;
        private Mover mover;

        private void Awake()
        {
            sound = gameObject.AddComponent<Sound>();
            mover = GetComponent<Mover>();

            sound.SoundSettings = settings;
        }

        private void OnEnable()
        {
            mover.OnMoveStart += Play;
        }

        private void OnDisable()
        {
            mover.OnMoveStart -= Play;
        }

        private void Play()
        {
            sound.Play(clips.Random());
        }
    }
}