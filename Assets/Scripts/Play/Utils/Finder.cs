﻿using Game;
using UnityEngine;

namespace Harmony
{
    public static partial class Finder
    {

        private static BunnyDeathEventChannel bunnyDeathEventChannel;
        public static BunnyDeathEventChannel BunnyDeathEventChannel
        {
            get
            {
                if (bunnyDeathEventChannel == null)
                {
                    GameObject mainController = GameObject.FindWithTag(Tags.MainController);
                    bunnyDeathEventChannel = mainController.GetComponent<BunnyDeathEventChannel>();
                }

                return bunnyDeathEventChannel;
            }
        }
        
        private static BunnyBirthEventChannel bunnyBirthEventChannel;
        public static BunnyBirthEventChannel BunnyBirthEventChannel
        {
            get
            {
                if (bunnyBirthEventChannel == null)
                {
                    GameObject mainController = GameObject.FindWithTag(Tags.MainController);
                    bunnyBirthEventChannel = mainController.GetComponent<BunnyBirthEventChannel>();
                }

                return bunnyBirthEventChannel;
            }
        }
        
        private static FoxDeathEventChannel foxDeathEventChannel;
        public static FoxDeathEventChannel FoxDeathEventChannel
        {
            get
            {
                if (foxDeathEventChannel == null)
                {
                    GameObject mainController = GameObject.FindWithTag(Tags.MainController);
                    foxDeathEventChannel = mainController.GetComponent<FoxDeathEventChannel>();
                }

                return foxDeathEventChannel;
            }
        }
        
        private static FoxBirthEventChannel foxBirthEventChannel;
        public static FoxBirthEventChannel FoxBirthEventChannel
        {
            get
            {
                if (foxBirthEventChannel == null)
                {
                    GameObject mainController = GameObject.FindWithTag(Tags.MainController);
                    foxBirthEventChannel = mainController.GetComponent<FoxBirthEventChannel>();
                }

                return foxBirthEventChannel;
            }
        }
        
        
        
        private static SceneBundleLoader findableSceneBundleLoader = null;

        public static SceneBundleLoader SceneBundleLoader
        {
            get
            {
                if (!findableSceneBundleLoader)
                {
                    findableSceneBundleLoader = FindWithTag<SceneBundleLoader>(Tags.MainController);
                }

                return findableSceneBundleLoader;
            }
        }

        private static SqLiteConnectionFactory findableSqLiteConnectionFactory = null;

        public static SqLiteConnectionFactory SqLiteConnectionFactory
        {
            get
            {
                if (!findableSqLiteConnectionFactory)
                {
                    findableSqLiteConnectionFactory = FindWithTag<SqLiteConnectionFactory>(Tags.MainController);
                }

                return findableSqLiteConnectionFactory;
            }
        }

        private static NavigationMesh findableNavigationMesh = null;

        public static NavigationMesh NavigationMesh
        {
            get
            {
                if (!findableNavigationMesh)
                {
                    findableNavigationMesh = FindWithTag<NavigationMesh>(Tags.NavigationMesh);
                }

                return findableNavigationMesh;
            }
        }

        private static VirtualCamera findableGameVirtualCamera = null;

        public static VirtualCamera GameVirtualCamera
        {
            get
            {
                if (!findableGameVirtualCamera)
                {
                    findableGameVirtualCamera = FindWithTag<VirtualCamera>(Tags.GameCamera);
                }

                return findableGameVirtualCamera;
            }
        }
    }
}