﻿using System.Data.Common;
using System.Data.SqlClient;
using System.Threading;
using Game;
using Harmony;
using UnityEngine;

namespace DataBase
{    
    public class SimulationStats : MonoBehaviour
    {
        private Repository repository;
        private SimulationObject simulationObject;
        
        private float startTime;
        private int currentTimeInSeconds;

        private BunnyBirthEventChannel bunnyBirthEventChannel;
        private BunnyDeathEventChannel bunnyDeathEventChannel;
        private FoxBirthEventChannel foxBirthEventChannel;
        private FoxDeathEventChannel foxDeathEventChannel;

        private DbConnection connection;
        
        //Author: Benoit Simon-Turgeon
        private void Awake()
        {
            connection = Finder.SqLiteConnectionFactory.GetConnection();

            startTime = Time.time;
            currentTimeInSeconds = 0;

            repository = new Repository(connection);

            repository.CreateTable();

            simulationObject = new SimulationObject(repository.FindLastId() + 1);

            bunnyBirthEventChannel = GetComponent<BunnyBirthEventChannel>();
            bunnyBirthEventChannel.OnBunnyBirth += OnBunnyBirth;

            bunnyDeathEventChannel = GetComponent<BunnyDeathEventChannel>();
            bunnyDeathEventChannel.OnBunnyDeath += OnBunnyDeath;
            
            foxBirthEventChannel = GetComponent<FoxBirthEventChannel>();
            foxBirthEventChannel.OnFoxBirth += OnFoxBirth;

            foxDeathEventChannel = GetComponent<FoxDeathEventChannel>();
            foxDeathEventChannel.OnFoxDeath += OnFoxDeath;
        }

        //Author: Benoit Simon-Turgeon
        private void Update()
        {
            if ((int) (Time.time - startTime) > currentTimeInSeconds)
            {
                currentTimeInSeconds = (int) (Time.time - startTime);
                simulationObject.Time = currentTimeInSeconds;
                try
                {
                    repository.Insert(simulationObject);
                }
                catch (SqlException sqlException)
                {
                    Debug.LogError(sqlException);
                }
            }
        }
        
        //Author: François-Xavier Bernier
        private void OnDestroy()
        {
            bunnyBirthEventChannel.OnBunnyBirth -= OnBunnyBirth;
            bunnyDeathEventChannel.OnBunnyDeath -= OnBunnyDeath;
            foxBirthEventChannel.OnFoxBirth -= OnFoxBirth;
            foxDeathEventChannel.OnFoxDeath -= OnFoxDeath;
        }
        //Author: François-Xavier Bernier
        private void OnBunnyBirth(Bunny bunny)
        {
            Thread.Sleep(1);
            simulationObject.addBunny();
        }
        //Author: François-Xavier Bernier
        private void OnBunnyDeath(Bunny bunny)
        {
            Thread.Sleep(1);
            simulationObject.removeBunny();
        }
        //Author: François-Xavier Bernier
        private void OnFoxBirth(Fox fox)
        {
            Thread.Sleep(1);
            simulationObject.addFox();
        }
        //Author: François-Xavier Bernier
        private void OnFoxDeath(Fox fox)
        {
            Thread.Sleep(1);
            simulationObject.removeFox();
        }
    }
}