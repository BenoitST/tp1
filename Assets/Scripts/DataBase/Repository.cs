﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Threading;

namespace DataBase
{
    public class Repository : IRepository
    {
        private DbConnection connection;

        //Author: Benoit Simon-Turgeon
        public Repository(DbConnection connection)
        {
            this.connection = connection;
            connection.Open();
        }

        //Author: Benoit Simon-Turgeon
        ~Repository()
        {
            if (connection.State != ConnectionState.Closed)
                connection?.Close();
        }
        
        //Author: François-Xavier Bernier
        public void Insert(SimulationObject simulationObject)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                InsertInBackground(simulationObject);
            }).Start();
        }

        //Author: François-Xavier Bernier
        public void Save(SimulationObject simulationObject)
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                SaveInBackground(simulationObject);
            }).Start();
        }

        //Author: François-Xavier Bernier
        public void CreateTable()
        {
            new Thread(() =>
            {
                Thread.CurrentThread.IsBackground = true;
                CreateTableThread();
            }).Start();
        }

        //Author: François-Xavier Bernier
        public void CreateTableThread()
        {
            using (var transaction = connection.BeginTransaction())
            {
                var command = connection.CreateCommand();

                command.CommandText =
                    "CREATE TABLE IF NOT EXISTS simulation( id INTEGER PRIMARY KEY NOT NULL ,seed INT NOT NULL,simulationId INT NOT NULL,time INT NOT NULL,nbFox INT NOT NULL,nbDeadFox INT NOT NULL,nbBunny INT NOT NULL,nbDeadBunny INT NOT NULL)";

                command.ExecuteNonQuery();
                transaction.Commit();
            }
        }

        //Author : Benoit Simon-Turgeon
        public void InsertInBackground(SimulationObject simulationObject)
        {
            var command = connection.CreateCommand();
            command.CommandText = "INSERT INTO simulation(seed, simulationId, time, nbFox, nbDeadFox, nbBunny, nbDeadBunny) VALUES(?,?,?,?,?,?,?)";

            var seedParameter = command.CreateParameter();
            seedParameter.Value = simulationObject.Seed;
            command.Parameters.Add(seedParameter);

            var simulationIdParameter = command.CreateParameter();
            simulationIdParameter.Value = simulationObject.SimulationNumber;
            command.Parameters.Add(simulationIdParameter);

            var timeParameter = command.CreateParameter();
            timeParameter.Value = simulationObject.Time;
            command.Parameters.Add(timeParameter);

            var nbFoxParameter = command.CreateParameter();
            nbFoxParameter.Value = simulationObject.NbFox;
            command.Parameters.Add(nbFoxParameter);

            var nbDeadFoxParameter = command.CreateParameter();
            nbDeadFoxParameter.Value = simulationObject.NbDeadFox;
            command.Parameters.Add(nbDeadFoxParameter);

            var nbBunnyParameter = command.CreateParameter();
            nbBunnyParameter.Value = simulationObject.NbBunny;
            command.Parameters.Add(nbBunnyParameter);

            var nbDeadBunnyParameter = command.CreateParameter();
            nbDeadBunnyParameter.Value = simulationObject.NbDeadBunny;
            command.Parameters.Add(nbDeadBunnyParameter);

            command.ExecuteNonQuery();
        }

        //Author: François-Xavier Bernier & Benoit Simon-Turgeon
        public SimulationObject Find(int idSimu)
        {
            SimulationObject finalResult = null;

            var command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM simulation WHERE id= idSimu";

            using (DbDataReader result = command.ExecuteReader())
            {
                if (result.HasRows)
                {
                    finalResult = new SimulationObject(0);
                    while (result.Read())
                    {
                        finalResult.Seed = result.GetInt32(1);
                        finalResult.SimulationNumber = result.GetInt32(2);
                        finalResult.Time = result.GetInt32(3);
                        finalResult.NbFox = result.GetInt32(4);
                        finalResult.NbDeadFox = result.GetInt32(5);
                        finalResult.NbBunny = result.GetInt32(6);
                        finalResult.NbDeadBunny = result.GetInt32(7);
                    }
                }
                result.Close();
            }

            return finalResult;
        }

        //Author : Benoit Simon-Turgeon
        public int FindLastId()
        {
            int finalResult = 0;

            var command = connection.CreateCommand();
            command.CommandText = "SELECT simulationId FROM simulation ORDER BY simulationId DESC LIMIT 1";
            using (DbDataReader result = command.ExecuteReader())
            {
                if (result.Read())
                {
                    finalResult = result.GetInt32(0);
                }

                result.Close();
            }

            return finalResult;
        }

        //Author : François-Xavier Bernier
        public List<SimulationObject> FindAll()
        {
            List<SimulationObject> allSimulationsObjects = null;
            var transaction = connection.BeginTransaction();
            var command = connection.CreateCommand();
            command.CommandText = "Select * FROM simulation";
            DbDataReader result = command.ExecuteReader();

            if (result.HasRows)
            {
                while (result.Read())
                {
                    SimulationObject simulationObjectToAdd = new SimulationObject(0);
                    simulationObjectToAdd.Seed = result.GetInt32(1);
                    simulationObjectToAdd.SimulationNumber = result.GetInt32(2);
                    simulationObjectToAdd.Time = result.GetInt32(3);
                    simulationObjectToAdd.NbFox = result.GetInt32(4);
                    simulationObjectToAdd.NbDeadFox = result.GetInt32(5);
                    simulationObjectToAdd.NbBunny = result.GetInt32(6);
                    simulationObjectToAdd.NbDeadBunny = result.GetInt32(7);
                    allSimulationsObjects.Add(simulationObjectToAdd);
                }
            }
            result.Close();
            return allSimulationsObjects;
        }

        //Author : François-Xavier Bernier
        public void SaveInBackground(SimulationObject simulationObject)
        {
            var transaction = connection.BeginTransaction();
            var command = connection.CreateCommand();
            /*
            command.CommandText = "UPDATE simulation SET seed = :seed, simulationId = simulationId; time = :time, nbFox = nbFox, nbDeadFox = nbDeadFox; nbBunny = nbBunny; nbDeadBunny = nbDeadBunny WHERE id =:id";
            command.Parameters.Add("seed", DbType.String).Value = textBox2.Text; 
            command.Parameters.Add("simulationId", DbType.String).Value = textBox3.Text; 
            command.Parameters.Add("time", DbType.String).Value = textBox4.Text; 
            command.Parameters.Add("nbFox", DbType.String).Value = textBox5.Text; 
            command.Parameters.Add("nbFoxDead", DbType.String).Value = textBox6.Text;
            command.Parameters.Add("nbBunny", DbType.String).Value = textBox7.Text; 
            command.Parameters.Add("nbDeadBunnt", DbType.String).Value = textBox7.Text;*/
            command.ExecuteNonQuery();

            transaction.Commit();
        }

        //Author : François-Xavier Bernier
        public void Delete(SimulationObject simulationObject)
        {
            using (var transaction = connection.BeginTransaction())
            {
                var command = connection.CreateCommand();
                command.CommandText = "DELETE FROM simulation WHERE simulationId =" +
                                      simulationObject.SimulationNumber.ToString();
                command.Parameters.Add(simulationObject.SimulationNumber);
                command.ExecuteNonQuery();
                command.ExecuteNonQuery();
                transaction.Commit();
            }
        }

        //Author : François-Xavier Bernier
        public void Delete(long id)
        {
            using (var transaction = connection.BeginTransaction())
            {
                var command = connection.CreateCommand();
                command.CommandText = "DELETE FROM simulation WHERE simulationId =" + id.ToString();
                command.Parameters.Add(id.ToString());
                command.ExecuteNonQuery();
                command.ExecuteNonQuery();
                transaction.Commit();
            }
        }
    }
}