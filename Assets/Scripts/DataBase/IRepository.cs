﻿using System.Collections.Generic;

namespace DataBase
{
    //Author: François-Xavier Bernier
    public interface IRepository
    {
        
        // Création - INSERT
        void Insert(SimulationObject simulationObject);

        //Recherche par le ID - SELECT
        SimulationObject Find(int seed);

        //Lire tout - SELECT ALL
        List<SimulationObject> FindAll();

        //Sauvegarde - UPDATE
        void Save(SimulationObject simulationObject);


        //Suppression - DELETE
        void Delete(SimulationObject simulationObject);

        //Suppression par ID - DELETE
        void Delete(long id);
    }
}