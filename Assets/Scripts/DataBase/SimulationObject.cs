﻿using Harmony;
using UnityEngine;
using UnityEngine.PlayerLoop;

namespace DataBase
{
    //Author : Benoit Simon-Turgeon
    public class SimulationObject
    {
        private int nbFox = 0;
        private int nbBunny = 0;
        private int nbDeadFox = 0;
        private int nbDeadBunny = 0;
        private int simulationNumber = 0;
        private float time = 0;
        private float seed;

        public float Time
        {
            get => time;
            set => time = value;
        }

        public float Seed
        {
            get => seed;
            set => seed = value;
        }

        public SimulationObject(int simulationNumber)
        {
            SimulationNumber = simulationNumber;
            seed = Finder.RandomSeed.Seed;
        }

        public int SimulationNumber
        {
            get => simulationNumber;
            set => simulationNumber = value;
        }

        public int NbFox
        {
            get => nbFox;
            set => nbFox = value;
        }

        public int NbBunny
        {
            get => nbBunny;
            set => nbBunny = value;
        }

        public int NbDeadFox
        {
            get => nbDeadFox;
            set => nbDeadFox = value;
        }

        public int NbDeadBunny
        {
            get => nbDeadBunny;
            set => nbDeadBunny = value;
        }

        public void addFox()
        {
            ++nbFox;
        }

        public void addBunny()
        {
            ++nbBunny;
        }

        public void removeBunny()
        {
            --nbBunny;
            ++nbDeadBunny;
        }

        public void removeFox()
        {
            --nbFox;
            ++nbDeadFox;
        }
    }
}