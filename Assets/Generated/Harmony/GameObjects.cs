// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-08-24 11:26:56

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

namespace Harmony
{
    public static partial class GameObjects
    {
        public const string MainCamera = "MainCamera"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Main.unity".
        public const string Plant = "Plant"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/Plant.prefab".
        public const string Stimuli = "Stimuli"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/World/Water.prefab".
        public const string Play = "Play"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Home.unity".
        public const string Music = "Music"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Main.unity".
        public const string GameCamera = "GameCamera"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public const string Tree = "Tree"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/Tree.prefab".
        public const string OffspringCreator = "OffspringCreator"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Animal.prefab".
        public const string Flora = "Flora"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public const string Title = "Title"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Home.unity".
        public const string EventSystem = "EventSystem"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Main.unity".
        public const string Text = "Text"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/UI/Button.prefab".
        public const string Animal = "Animal"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Animal.prefab".
        public const string Mover = "Mover"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Animal.prefab".
        public const string Fox = "Fox"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Fox.prefab".
        public const string Anchor = "Anchor"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Home.unity".
        public const string Fauna = "Fauna"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public const string Button = "Button"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/UI/Button.prefab".
        public const string Home = "Home"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Home.unity".
        public const string RockB = "RockB"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Rocks/RockB.prefab".
        public const string FirB = "FirB"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/FirB.prefab".
        public const string Vitals = "Vitals"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Animal.prefab".
        public const string GrassA = "GrassA"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/GrassA.prefab".
        public const string MainController = "MainController"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Main.unity".
        public const string Sun = "Sun"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public const string Quit = "Quit"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Home.unity".
        public const string GameController = "GameController"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public const string Terrain = "Terrain"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public const string Fern = "Fern"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/Fern.prefab".
        public const string NavigationMesh = "NavigationMesh"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public const string Feeder = "Feeder"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Animal.prefab".
        public const string Ambient = "Ambient"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public const string Water = "Water"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public const string FirA = "FirA"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/FirA.prefab".
        public const string RockA = "RockA"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Rocks/RockA.prefab".
        public const string GrassB = "GrassB"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/GrassB.prefab".
        public const string Sensor = "Sensor"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Animal.prefab".
        public const string Bunny = "Bunny"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Bunny.prefab".
        public const string Mesh = "Mesh"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/FirA.prefab".
    }
}