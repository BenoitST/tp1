// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-08-24 11:26:56

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public partial class Prefabs : MonoBehaviour
    {
        private static Prefabs instance;
    
        [SerializeField] private GameObject FirAPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/FirA.prefab".
        [SerializeField] private GameObject TreePrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/Tree.prefab".
        [SerializeField] private GameObject ButtonPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/UI/Button.prefab".
        [SerializeField] private GameObject FernPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/Fern.prefab".
        [SerializeField] private GameObject GrassAPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/GrassA.prefab".
        [SerializeField] private GameObject FoxPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Fox.prefab".
        [SerializeField] private GameObject RockBPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Rocks/RockB.prefab".
        [SerializeField] private GameObject GrassBPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/GrassB.prefab".
        [SerializeField] private GameObject BunnyPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Bunny.prefab".
        [SerializeField] private GameObject FirBPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/FirB.prefab".
        [SerializeField] private GameObject RockAPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Rocks/RockA.prefab".
        [SerializeField] private GameObject AnimalPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Animal.prefab".
        [SerializeField] private GameObject PlantPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/Plant.prefab".
        [SerializeField] private GameObject WaterPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/World/Water.prefab".
    
        public static GameObject FirA => instance.FirAPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/FirA.prefab".
        public static GameObject Tree => instance.TreePrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/Tree.prefab".
        public static GameObject Button => instance.ButtonPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/UI/Button.prefab".
        public static GameObject Fern => instance.FernPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/Fern.prefab".
        public static GameObject GrassA => instance.GrassAPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/GrassA.prefab".
        public static GameObject Fox => instance.FoxPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Fox.prefab".
        public static GameObject RockB => instance.RockBPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Rocks/RockB.prefab".
        public static GameObject GrassB => instance.GrassBPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/GrassB.prefab".
        public static GameObject Bunny => instance.BunnyPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Bunny.prefab".
        public static GameObject FirB => instance.FirBPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Trees/FirB.prefab".
        public static GameObject RockA => instance.RockAPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Rocks/RockA.prefab".
        public static GameObject Animal => instance.AnimalPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Actors/Animal.prefab".
        public static GameObject Plant => instance.PlantPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/Nature/Grass/Plant.prefab".
        public static GameObject Water => instance.WaterPrefab; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Prefabs/World/Water.prefab".

        public Prefabs()
        {
            instance = this;
        }
    }
}