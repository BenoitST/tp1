// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-08-24 11:26:56

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine.SceneManagement;

namespace Harmony
{
    public static partial class Scenes
    {
        public static readonly Scene Game = SceneManager.GetSceneByName("Game"); // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Game.unity".
        public static readonly Scene Home = SceneManager.GetSceneByName("Home"); // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Home.unity".
        public static readonly Scene Main = SceneManager.GetSceneByName("Main"); // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scenes/Main.unity".
    }
}