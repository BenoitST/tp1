// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-08-24 11:26:56

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using UnityEngine;

namespace Harmony
{
    public static partial class Layers
    {
        public static readonly LayerMask UI = LayerMask.NameToLayer("UI"); // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public static readonly LayerMask Sensor = LayerMask.NameToLayer("Sensor"); // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public static readonly LayerMask TransparentFX = LayerMask.NameToLayer("TransparentFX"); // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        // "Ignore Raycast" is invalid. See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public static readonly LayerMask Water = LayerMask.NameToLayer("Water"); // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public static readonly LayerMask Default = LayerMask.NameToLayer("Default"); // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
    }
}