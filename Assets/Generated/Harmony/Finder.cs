// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-08-24 11:26:56

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

using System;
using UnityEngine;

namespace Harmony
{
    public static partial class Finder
    {
        private static Game.WaterGenerator findableWaterGenerator = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Water/WaterGenerator.cs".
        public static Game.WaterGenerator WaterGenerator
        {
            get
            {
                if (!findableWaterGenerator)
                {
                    findableWaterGenerator = FindWithTag<Game.WaterGenerator>(Tags.Water);
                }
                return findableWaterGenerator;
            }
        }
    
        private static Game.Terrain findableTerrain = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Terrain/Terrain.cs".
        public static Game.Terrain Terrain
        {
            get
            {
                if (!findableTerrain)
                {
                    findableTerrain = FindWithTag<Game.Terrain>(Tags.Terrain);
                }
                return findableTerrain;
            }
        }
    
        private static Game.TerrainAnimator findableTerrainAnimator = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Terrain/TerrainAnimator.cs".
        public static Game.TerrainAnimator TerrainAnimator
        {
            get
            {
                if (!findableTerrainAnimator)
                {
                    findableTerrainAnimator = FindWithTag<Game.TerrainAnimator>(Tags.Terrain);
                }
                return findableTerrainAnimator;
            }
        }
    
        private static Game.TerrainGenerator findableTerrainGenerator = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Terrain/TerrainGenerator.cs".
        public static Game.TerrainGenerator TerrainGenerator
        {
            get
            {
                if (!findableTerrainGenerator)
                {
                    findableTerrainGenerator = FindWithTag<Game.TerrainGenerator>(Tags.Terrain);
                }
                return findableTerrainGenerator;
            }
        }
    
        private static Game.NavigationMeshGenerator findableNavigationMeshGenerator = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Navigation/NavigationMeshGenerator.cs".
        public static Game.NavigationMeshGenerator NavigationMeshGenerator
        {
            get
            {
                if (!findableNavigationMeshGenerator)
                {
                    findableNavigationMeshGenerator = FindWithTag<Game.NavigationMeshGenerator>(Tags.NavigationMesh);
                }
                return findableNavigationMeshGenerator;
            }
        }
    
        private static Game.PathFinder findablePathFinder = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Navigation/PathFinder.cs".
        public static Game.PathFinder PathFinder
        {
            get
            {
                if (!findablePathFinder)
                {
                    findablePathFinder = FindWithTag<Game.PathFinder>(Tags.NavigationMesh);
                }
                return findablePathFinder;
            }
        }
    
        private static Game.Flora findableFlora = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Flora/Flora.cs".
        public static Game.Flora Flora
        {
            get
            {
                if (!findableFlora)
                {
                    findableFlora = FindWithTag<Game.Flora>(Tags.Flora);
                }
                return findableFlora;
            }
        }
    
        private static Game.FloraAnimator findableFloraAnimator = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Flora/FloraAnimator.cs".
        public static Game.FloraAnimator FloraAnimator
        {
            get
            {
                if (!findableFloraAnimator)
                {
                    findableFloraAnimator = FindWithTag<Game.FloraAnimator>(Tags.Flora);
                }
                return findableFloraAnimator;
            }
        }
    
        private static Game.FloraGenerator findableFloraGenerator = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Flora/FloraGenerator.cs".
        public static Game.FloraGenerator FloraGenerator
        {
            get
            {
                if (!findableFloraGenerator)
                {
                    findableFloraGenerator = FindWithTag<Game.FloraGenerator>(Tags.Flora);
                }
                return findableFloraGenerator;
            }
        }
    
        private static Game.FaunaAnimator findableFaunaAnimator = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Fauna/FaunaAnimator.cs".
        public static Game.FaunaAnimator FaunaAnimator
        {
            get
            {
                if (!findableFaunaAnimator)
                {
                    findableFaunaAnimator = FindWithTag<Game.FaunaAnimator>(Tags.Fauna);
                }
                return findableFaunaAnimator;
            }
        }
    
        private static Game.FaunaGenerator findableFaunaGenerator = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/World/Fauna/FaunaGenerator.cs".
        public static Game.FaunaGenerator FaunaGenerator
        {
            get
            {
                if (!findableFaunaGenerator)
                {
                    findableFaunaGenerator = FindWithTag<Game.FaunaGenerator>(Tags.Fauna);
                }
                return findableFaunaGenerator;
            }
        }
    
        private static Game.PrefabFactory findablePrefabFactory = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/Utils/PrefabFactory.cs".
        public static Game.PrefabFactory PrefabFactory
        {
            get
            {
                if (!findablePrefabFactory)
                {
                    findablePrefabFactory = FindWithTag<Game.PrefabFactory>(Tags.MainController);
                }
                return findablePrefabFactory;
            }
        }
    
        private static Game.RandomSeed findableRandomSeed = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/Utils/Random/RandomSeed.cs".
        public static Game.RandomSeed RandomSeed
        {
            get
            {
                if (!findableRandomSeed)
                {
                    findableRandomSeed = FindWithTag<Game.RandomSeed>(Tags.MainController);
                }
                return findableRandomSeed;
            }
        }
    
        private static Game.Main findableMain = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/Mode/Main/Main.cs".
        public static Game.Main Main
        {
            get
            {
                if (!findableMain)
                {
                    findableMain = FindWithTag<Game.Main>(Tags.MainController);
                }
                return findableMain;
            }
        }
    
        private static Game.Inputs findableInputs = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/Input/Inputs.cs".
        public static Game.Inputs Inputs
        {
            get
            {
                if (!findableInputs)
                {
                    findableInputs = FindWithTag<Game.Inputs>(Tags.MainController);
                }
                return findableInputs;
            }
        }
    
        private static Game.Ambient findableAmbient = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/Audio/Ambient.cs".
        public static Game.Ambient Ambient
        {
            get
            {
                if (!findableAmbient)
                {
                    findableAmbient = FindWithTag<Game.Ambient>(Tags.Ambient);
                }
                return findableAmbient;
            }
        }
    
        private static Game.Music findableMusic = null; //See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/Assets/Scripts/Play/Audio/Music.cs".
        public static Game.Music Music
        {
            get
            {
                if (!findableMusic)
                {
                    findableMusic = FindWithTag<Game.Music>(Tags.Music);
                }
                return findableMusic;
            }
        }
    
        public static T FindWithTag<T>(string tag) where T : class
        {
            return GameObject.FindWithTag(tag)?.GetComponent<T>();
        }
    }
}