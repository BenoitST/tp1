// AUTO GENERATED CODE - ANY MODIFICATION WILL BE OVERRIDEN
// GENERATED ON 2020-08-24 11:26:56

// Invalid names are commented out. Here are the rules :
// - Non-alphanumerical characters (like spaces) are prohibited. Underscores are allowed.
// - Per C# language rules, starting with a non alphabetic character is prohibited.
// - Per C# language rules, using the same name as it's class is prohibited. Ex : "GameObjects", "Tags" or "Layers".
// - Per C# language rules, using a keyword is prohibited. Ex : "object", "abstract" or "float".

namespace Harmony
{
    public static partial class Tags
    {
        public const string Ambient = "Ambient"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string EditorOnly = "EditorOnly"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string MainCamera = "MainCamera"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string GameCamera = "GameCamera"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string Flora = "Flora"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string MainController = "MainController"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string Water = "Water"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string Finish = "Finish"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string Respawn = "Respawn"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string Terrain = "Terrain"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string GameController = "GameController"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string Untagged = "Untagged"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string Fauna = "Fauna"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string Music = "Music"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string NavigationMesh = "NavigationMesh"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
        public const string Player = "Player"; // See "/mnt/donnees/Users/Utilisateur/Documents/Repos/ProjetSynthese/tp1/ProjectSettings/TagManager.asset".
    }
}